<?php
require_once 'zfbootstrap.php';
require_once 'functions.php';

$feedModel = new Application_Model_Feed();


$sites = array(
    "http://www.masnun.me/feed",
    "http://hasin.me/feed/"
);

$mergedFeed = array(
    'title' => 'ArikFr.com merged Feed',
    'link' => 'http://www.arikfr.com/merged_feed.php',
    'charset' => 'UTF-8',
);

$allEntries = array();
foreach ($sites as $uri)
{
    $entries = $feedModel->uriToArray($uri);
    foreach ($entries as $entry)
    {
        $allEntries[] = $entry;
    }
}

usort($allEntries, 'handleSortingOrder');

$mergedFeed['entries'] = $allEntries;

$output = Zend_Feed::importArray($mergedFeed, 'rss');
$output->send();



<?php
function handleSortingOrder($a, $b)
{
    $a_time = $a['lastUpdate'];
    $b_time = $b['lastUpdate'];

    if ($a_time == $b_time)
    {
        return 0;
    }

    return ($a_time > $b_time) ? -1 : 1;

}
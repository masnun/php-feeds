<?php

class TestController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout->disableLayout();
    }

    public function indexAction()
    {
        $feed = Zend_Feed::import("http://www.masnun.me/feed");
        foreach($feed as $entry) {
            echo $entry->title()."<br/><br/>";
            echo $entry->link()."<br/><br/>";
            echo $entry->description()."<br/><br/>";
        }
    }


}


<?php

class Application_Model_Feed
{

    public function uriToArray($uri)
    {
        try
        {

            $feed = Zend_Feed::import($uri);
            $entries = array();

            foreach ($feed as $entry)
            {
                $entries[] = array(
                    'title' => $entry->title(),
                    'link' => $entry->link(),
                    'guid' => $entry->guid(),
                    'lastUpdate' => strtotime($entry->pubDate()),
                    'description' => $entry->description(),
                    'pubDate' => $entry->pubDate(),

                );

            }

            return $entries;

        } catch (Exception $e)
        {
            print "Exception: " . $e->getMessage() . "\n";
            return FALSE;
        }

    }

}

